---
layout: post
title:  "Silence"
image: silence.jpg
date:   2019-07-19 20:04:01 +0100
categories: jekyll update
---

Shadows licked slowly across the floor. Long lingering tongues, languid, lazy creatures, feasting on the last bits of daylight. Slithering beyond into night. Pitch, an inkiness that swallowed the world outside, enveloped the house. Enveloped the world.

Was there a world beyond these walls?

Could it really be so simple?

The quiet that settled was all consuming. A forever silence. There was Nothing here. Always Nothing. Unearthly, unassuming, ghastly in it’s perfection, watching. Nothing.

It was time.

She woke. Stood, stretched from her settled position on her high backed chair. Once, perhaps, it had been a beautiful work of art. Glossy with paint, covered in red velvet fabric, carved intricately from end to end. Yet now, faded, chipped, it sagged with pain as she slid away. As if it missed her weight, or was glad to have her weight gone.

Silence. Gloomy black followed her, moving with her. Room to room as she glided to the water. To the night room. To where the Nothing waited for her. Her hair tumbled down her back. Annoying her. Her fingers began to work the fly away strands, the bulk braiding easily enough, until she was able to drape it across her back like a noose she could never use.

If only she could hear the sound of bird song again. The sound of a child’s laughter once more. The wind rushing through trees. If only for a moment. To escape the Nothing that trapped her here. Freed her into an everlasting night. To wake before the dawn of shadows at her feet. To remember what it was to be real again.

To be something other than a china doll.

A play thing.

Her water. It glowed with ethereal attraction. To pull her in. To pull them all in. The forever night that would bring them to the Nothing. Take them to the Beyond.

The dress she wore was old. How old? Ten years? Twenty? A hundred? It stuck as she tried to pull at the strings that bound it to her body. She shifted, yanked, it tore. She cursed, but no sound came. As always.

Had it always been that way? Had there ever been another way? Had the silence engulfed her so long ago that she had forgotten? The memories were too faded. Too confused. She screamed, her mouth open in a silent O of agonised pain. Her fingers grasped the fabric, jerking it, tearing it from her body. It shredded like tissue, dropping to the floor in a heap.

Salt. Tears had stained her cheeks and she could taste her torment. Her bewilderment. Little by little her body began shaking with huge, heavy, muted sobs. Still, the Nothing was there, her silent weeping weird sort of pantomime of anguish.

Twilight grew darker. It was growing late. She picked up the heap of torn fabric, ashamed at her outburst. The deep, tortured grief growing numb once more. The memories may never surface, but time would continue to tick on, and Nothing would not wait. She slid into the water. Exquisite in her beauty. Dangerous in her allure.

The water was so achingly hot, but she felt so very cold.

Nothing would not wait. The silence had to be fed.
