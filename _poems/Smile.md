---
layout: pile
title:  "Smile"
image: Slips.png
date:   2019-07-11 20:04:01 +0100
categories: jekyll update
---

She melts across the horizon
The golden hue of her hair dripping ink in the distance
Dusty tint of obsidian my friend

Still, every morning she greets me
The heat of her compassion awakens us all
And brightens the sky once more
