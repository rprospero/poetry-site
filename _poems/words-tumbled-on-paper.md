---
layout: pile
title:  "Words Tumbled On Paper"
image: Awaken.png
date:   2019-07-10 20:04:01 +0100
categories: jekyll update
---

He sits
On a burgeoning pile
of discarded dreams
lost adrift
my lover screams
screams

Rising high
Cursed crimson moon
His honied lips
promises bliss
my tenuous grasps slips
slips
